import type { IAlumno } from "@/interfaces/IAlumno";
import { ref } from 'vue' // función de reactividad
import type { Ref } from 'vue' // La interfaz de reactividad (ref)
const API_URL = 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos' //http://localhost:4200/api/v1 //https://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos

export default class AlumnoService {
    private alumnos: Ref<IAlumno[]>;
    private alumno: Ref<IAlumno>

    constructor() {
        this.alumnos = ref([])
        this.alumno = ref({}) as Ref<IAlumno>
    }
    //Son Los getters
    getUsers(): Ref<IAlumno[]> {
        return this.alumnos
    }
    getUser(): Ref<IAlumno> {
        return this.alumno
    }

    async fetchAll(): Promise<void> {
        try {
            const json = await fetch(`${API_URL}`)
            const response = await json.json()
            this.alumnos.value = response;
        } catch (error) {
            console.log(error)
        }
    }
    async fetchAlumnoByID(id: string): Promise<void> {
        try {
            const json = await fetch(`${API_URL}/${id}`)
            const response = await json.json()
            this.alumno.value = await response
        } catch (error) {
            console.log(error)
        }
    }
    async fetchPost(name: string, email: string,  group: string): Promise<void> {
        try {
            const json = await fetch(`${API_URL}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ name, email, group })
            })
            const response = await json.json();
            console.log(response);

        } catch (error) {
            console.log(error);
        }
    }
    async fetchDelete(id: number): Promise<void> {
        try {
            const json = await fetch(`${API_URL}/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const response = await json.json();
            console.log(response);
        } catch (error) {
            console.log(error);
        }
    }
}