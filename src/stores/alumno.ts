import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'
import AlumnoService from '@/services/AlumnoService';
import type { IAlumno } from '@/interfaces/IAlumno';

export const useAlumnoStore = defineStore('alumno', () => {
    //state
    const service = ref(new AlumnoService());
    const alumnos = ref([]) as Ref<IAlumno[]>
    const alumno = ref({}) as Ref<IAlumno>

    //actions
    async function getAlumnos() {
        await service.value.fetchAll();
        alumnos.value = service.value.getUsers().value;
    }
    async function getAlumnoByID(id: string) {
        await service.value.fetchAlumnoByID(id);
        alumno.value = service.value.getUser().value;
    }
    async function postAlumno(name: string, email: string, group: string) {
        await service.value.fetchPost(name, email, group);
        alumnos.value = service.value.getUsers().value;
    }
    async function deleteAlumno(id: number) {
        await service.value.fetchDelete(id);
        await service.value.fetchAll();
        alumnos.value = service.value.getUsers().value;
    }

    //getters
    return {
        alumnos,
        alumno,
        getAlumnos,
        getAlumnoByID,
        postAlumno,
        deleteAlumno,
    }

});
