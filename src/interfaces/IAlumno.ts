export interface IAlumno {
    id: number,
    name: string,
    email: string,
    // password: string,
    group: string
}